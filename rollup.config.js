import typescript from 'rollup-plugin-typescript2';
import { terser } from 'rollup-plugin-terser';
import postcss from 'rollup-plugin-postcss';
import PostcssUrl from 'postcss-url';

import pkg from './package.json';

module.exports = {
  input: './lib/index.ts',
  output: [
    {
      file: pkg.main,
      format: 'cjs',
    },
    {
      file: pkg.module,
      format: 'es',
    },
    {
      file: pkg.browser,
      format: 'iife',
      name: 'EmailsEditor',
    },
  ],
  plugins: [
    postcss({
      minimize: true,
      modules: true,
      inject: true,
      extensions: ['.css'],
      plugins: [
        new PostcssUrl({
          url: 'inline',
        }),
      ],
    }),
    typescript(),
    terser(),
  ],
};
