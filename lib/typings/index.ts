declare module '*.css';

declare namespace EmailsEditor {
  export type HTMLElementEvent<T extends HTMLElement> = Event & {
    target: T;
    currentTarget: T;
  };

  export const enum KeyCode {
    ENTER = 13,
    SPACE = 32,
    COMMA = 188,
  }

  export const enum Events {
    EMAILS_CHANGE = 'EMAILS_CHANGE_EVENT',
  }
}
