import { TextArea } from 'components/TextArea';
import { ButtonsBlock } from 'components/ButtonsBlock';

import styles from './index.css';

interface Options {
  container: HTMLElement;
  boardName?: string;
  placeholder?: string;
  initialValues?: string[];
}

interface DomElementOptions extends DOMStringMap {
  boardname?: string;
  placeholder?: string;
  initialvalues?: string;
}

const defaultOptions: Required<Pick<Options, 'boardName' | 'placeholder' | 'initialValues'>> = {
  boardName: 'Board name',
  initialValues: [],
  placeholder: 'add more people...',
};

class App extends HTMLElement {
  public dataset!: DomElementOptions;
  private options: Options & typeof defaultOptions;
  private textArea!: TextArea;

  constructor(options: Options) {
    super();
    let parsedOptions = options;

    if (this.dataset) {
      parsedOptions = {
        boardName: this.dataset.boardname || defaultOptions.boardName,
        initialValues: this.dataset.initialvalues
          ? this.dataset.initialvalues.split(/,\s*/)
          : defaultOptions.initialValues,
        placeholder: this.dataset.placeholder || defaultOptions.placeholder,
        ...options,
      };
    }

    this.options = {
      ...defaultOptions,
      ...parsedOptions,
    };

    this.render();
  }

  public setEmails(emails: string[], append = false) {
    this.textArea.setEmails(emails, append);
  }

  public getEmails(onlyValid = false) {
    return this.textArea.getEmails(onlyValid);
  }

  render() {
    const { boardName, placeholder, initialValues, container } = this.options;

    const fragment = document.createDocumentFragment();

    const title = document.createElement('div');
    const textLeft = document.createTextNode('Share ');
    const textRight = document.createTextNode(' with others');
    const boardNameText = document.createElement('span');

    title.className = styles.title;
    boardNameText.className = styles.boardName;
    boardNameText.textContent = boardName.trim();
    title.appendChild(textLeft);
    title.appendChild(boardNameText);
    title.appendChild(textRight);

    const form = document.createElement('div');
    form.className = styles.form;
    form.appendChild(title);
    const textArea = new TextArea({
      container: form,
      initialValues,
      placeholder,
      className: styles.textArea,
    });
    this.textArea = textArea;

    fragment.appendChild(form);
    fragment.appendChild(
      ButtonsBlock({
        onAdd: email => {
          textArea.setEmails([email], true);
        },
        onGetCount: () => {
          alert(`${this.getEmails().length} valid emails`);
        },
        className: styles.buttonsBlock,
      })
    );

    if (container) {
      container.classList.add(styles.root);
      container.appendChild(fragment);
    } else {
      this.classList.add(styles.root);
      this.appendChild(fragment);
    }
  }
}

customElements.define('emails-editor', App);

export default function(...args: ConstructorParameters<typeof App>) {
  return new App(...args);
}
