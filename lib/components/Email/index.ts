import { cn } from 'utils/classnames';

import styles from './styles.css';

interface Props {
  value: string;
  isValid?: boolean;
  onDelete: (email: string) => void;
  className?: string;
}

export const Email = (props: Props) => {
  const { value, isValid = true, onDelete, className } = props;

  const deleteButton = document.createElement('div');
  deleteButton.className = styles.delete;
  deleteButton.onclick = () => {
    onDelete(value);
  };

  const wrapper = document.createElement('div');
  wrapper.className = cn(styles.email, className, isValid ? styles.valid : styles.error);
  wrapper.textContent = value;

  wrapper.appendChild(deleteButton);

  return wrapper;
};
