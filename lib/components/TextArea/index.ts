import { cn } from 'utils/classnames';
import { Email } from 'components/Email';

import styles from './styles.css';
import { unique, diff } from 'utils/array';

interface Props {
  container: HTMLElement;
  initialValues?: string[];
  placeholder?: string;
  className?: string;
}

const ATTR_NAME = 'data-placeholder';

export class TextArea {
  private emails = new Map<string, boolean>();
  private textArea = document.createElement('div');
  private validator = document.createElement('input');

  constructor(private props: Props) {
    const { container, className, placeholder = '', initialValues = [] } = props;

    this.textArea.className = cn(styles.textArea, className);

    this.textArea.setAttribute(ATTR_NAME, placeholder);
    this.textArea.contentEditable = 'true';
    this.textArea.onkeyup = this.handleKeys;

    this.textArea.onfocus = this.onFocus;
    this.textArea.onblur = this.onBlur;

    this.validator.setAttribute('type', 'email');

    this.setEmails(initialValues);
    container.appendChild(this.textArea);
  }

  public setEmails(emails: string[], append = true) {
    const prev = Array.from(this.emails.keys());
    if (!append) {
      this.emails.clear();
    }

    emails.forEach(value => {
      this.emails.set(value, true);
    });
    const next = Array.from(this.emails.keys());

    this.render();
    this.fireChange(prev, next);
  }

  public getEmails(onlyValid = true) {
    const allEmails = Array.from(this.emails.keys());

    return !onlyValid ? allEmails : allEmails.filter(email => this.emails.get(email));
  }

  public getEmailsCount() {
    return this.getEmails().length;
  }

  private handleKeys = (event: KeyboardEvent) => {
    if (
      event.keyCode === EmailsEditor.KeyCode.ENTER ||
      event.keyCode === EmailsEditor.KeyCode.COMMA ||
      event.keyCode === EmailsEditor.KeyCode.SPACE
    ) {
      this.recomputeEmails();
      this.placeCaretAtTheEnd();

      if (
        event.keyCode === EmailsEditor.KeyCode.ENTER ||
        event.keyCode === EmailsEditor.KeyCode.COMMA
      ) {
        event.preventDefault();
      }
    }
  };

  private fireChange(prevEmails: string[], nextEmails: string[]) {
    const { container } = this.props;

    container.dispatchEvent(
      new CustomEvent(EmailsEditor.Events.EMAILS_CHANGE, {
        bubbles: true,
        cancelable: true,
        detail: [prevEmails, nextEmails, diff(prevEmails, nextEmails)],
      })
    );
  }

  private onFocus = () => {
    this.placeCaretAtTheEnd();
    this.textArea.removeAttribute(ATTR_NAME);
  };

  private onBlur = () => {
    const { placeholder = '' } = this.props;

    this.textArea.setAttribute(ATTR_NAME, placeholder);
    this.recomputeEmails();
  };

  private recomputeEmails() {
    const emails = this.textArea.innerText.trim().split(/,?\s+|,/);

    if (emails.length !== this.emails.size || emails.some(email => !this.emails.has(email))) {
      this.setEmails(unique(emails).filter(Boolean), false);
      this.render();
    }
  }

  private placeCaretAtTheEnd() {
    const selection = document.getSelection();

    if (selection) {
      selection.collapse(this.textArea, this.textArea.childElementCount);
    }
  }

  private onDelete = (email: string) => {
    const prev = Array.from(this.emails.keys());
    this.emails.delete(email);
    const next = Array.from(this.emails.keys());

    this.render();
    this.fireChange(prev, next);
    this.placeCaretAtTheEnd();
  };

  private renderEmails() {
    const fragment = document.createDocumentFragment();

    for (const [email] of this.emails) {
      this.validator.value = email;

      const emailBlock = Email({
        value: email,
        isValid: this.validator.validity.valid,
        className: styles.email,
        onDelete: this.onDelete,
      });
      emailBlock.contentEditable = 'false';
      this.emails.set(email, this.validator.validity.valid);

      fragment.appendChild(emailBlock);
    }

    return fragment;
  }

  render() {
    this.textArea.innerText = '';

    this.textArea.appendChild(this.renderEmails());
  }
}
