import { cn } from 'utils/classnames';

import styles from './styles.css';

interface ButtonProps {
  title: string;
  onClick: () => void;
  className?: string;
}

export const Button = (props: ButtonProps) => {
  const { title, onClick, className } = props;

  const button = document.createElement('button');
  const text = document.createTextNode(title);

  button.appendChild(text);
  button.onclick = onClick;
  button.className = cn(styles.button, className);

  return button;
};
