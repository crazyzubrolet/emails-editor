import { Button } from 'components/Button';
import { cn } from 'utils/classnames';
import { generateRandomEmail } from 'utils/email';

import styles from './styles.css';

interface Props {
  onAdd: (email: string) => void;
  onGetCount: () => void;
  className?: string;
}

export const ButtonsBlock = (props: Props) => {
  const { onAdd, onGetCount, className } = props;

  const addEmailButton = Button({
    title: 'Add email',
    onClick: () => {
      const email = generateRandomEmail();
      onAdd(email);
    },
    className: styles.button,
  });
  const getEmailsCount = Button({
    title: 'Get emails count',
    onClick: () => {
      if (onGetCount) {
        onGetCount();
      }
    },
    className: styles.button,
  });

  const buttonsBlock = document.createElement('div');
  buttonsBlock.className = cn(styles.buttonsBlock, className);

  buttonsBlock.appendChild(addEmailButton);
  buttonsBlock.appendChild(getEmailsCount);

  return buttonsBlock;
};
