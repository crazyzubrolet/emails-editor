import { cn } from './index';

describe('classnames utility', () => {
  it('should return spaced string for different number or arguments', () => {
    const class1 = 'test';
    const class2 = 'temporary';
    const class3 = 'classname';

    expect(cn(class1)).toBe(class1);
    expect(cn(class1, class2)).toBe(`${class1} ${class2}`);
    expect(cn(class1, class2, class3)).toBe(`${class1} ${class2} ${class3}`);
  });

  it('should filter falsy arguments from result string', () => {
    const class1 = 'valid';
    const class2 = '';
    const class3 = 0;
    const class4 = undefined;
    const class5 = null;

    expect(cn(class1, class2, class3, class4, class5)).toBe('valid');
  });
});
