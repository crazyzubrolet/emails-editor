export function cn(...classNames: Array<unknown>) {
  return classNames
    .filter((className): className is string => typeof className === 'string' && !!className)
    .join(' ');
}
