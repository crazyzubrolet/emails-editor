import { generateRandomEmail } from './index';

describe('Email utilities', () => {
  describe('generateRandomEmail', () => {
    const input = document.createElement('input');
    input.type = 'email';

    it.each<[string, boolean]>([
      [generateRandomEmail(), true],
      [generateRandomEmail(), true],
      [generateRandomEmail(), true],
    ])('generated email %s should be valid', (email, result) => {
      input.value = email;
      expect(input.validity.valid).toBe(result);
    });

    it('should generate random emails', () => {
      const email1 = generateRandomEmail();
      const email2 = generateRandomEmail();
      const email3 = generateRandomEmail();

      expect(email1).not.toEqual(email2);
      expect(email2).not.toEqual(email3);
      expect(email1).not.toEqual(email3);
    });
  });
});
