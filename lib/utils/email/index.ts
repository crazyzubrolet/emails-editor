const chars = 'abcdefghijklmnopqrstuvwxyz1234567890';
const zones = ['ru', 'com', 'net'];

export function generateRandomEmail() {
  const firstNameLength = Math.round(Math.random() * 10) + 2;
  const domainLength = Math.round(Math.random() * 5) + 2;

  let email = '';

  for (let i = 0; i < firstNameLength; i++) {
    email += chars[Math.floor(Math.random() * chars.length)];
  }

  email = `${email}@`;

  for (let i = 0; i < domainLength; i++) {
    email += chars[Math.floor(Math.random() * chars.length)];
  }

  const zone = zones[Math.floor(Math.random() * zones.length)];

  return `${email}.${zone}`;
}
