export function unique<T = unknown>(array: T[]) {
  return array.filter((item, index) => array.indexOf(item) === index);
}

export function diff<T = unknown>(array1: T[], array2: T[]) {
  const diff = array1.slice();

  array2.forEach(item => {
    const index = diff.indexOf(item);

    if (index === -1) {
      diff.push(item);
    } else {
      diff.splice(index, 1);
    }
  });

  return diff;
}
