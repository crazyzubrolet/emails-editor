import { unique, diff } from './index';

describe('Array utilities', () => {
  describe('unique', () => {
    const obj1 = { field: 'hello' };
    const obj2 = { ...obj1 };

    it.each<(unknown[])[]>([
      [[1, 2, 3, 4], [1, 2, 3, 4]],
      [[1, 1, 2, 3, 1], [1, 2, 3]],
      [['welcome', 'to the', 'test', 'suit'], ['welcome', 'to the', 'test', 'suit']],
      [[obj1, obj1], [obj1]],
      [[obj1, obj2], [obj1, obj2]],
    ])('for array %s should return %s', (initial, actual) => {
      expect(unique(initial)).toEqual(actual);
    });
  });

  describe('diff', () => {
    const obj1 = { some: 'field' };
    const obj2 = { ...obj1 };

    it.each<[unknown[], unknown[], unknown[]]>([
      [[1, 2, 3], [4, 5, 6], [1, 2, 3, 4, 5, 6]],
      [[1, 2, 3], [1, 2, 3], []],
      [[2, 4, 6], [4, 6, 8], [2, 8]],
      [['foo', 'bar'], ['foo'], ['bar']],
      [[obj1, obj2, obj1], [obj1], [obj1, obj2]],
    ])('should return diff between %s and %s with values %s', (arr1, arr2, actual) => {
      expect(diff(arr1, arr2)).toEqual(actual);
    });
  });
});
