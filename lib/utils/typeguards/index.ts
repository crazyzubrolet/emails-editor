export function isEventHasTarget<T extends HTMLElement>(
  event: Event
): event is EmailsEditor.HTMLElementEvent<T> {
  return !!event.target && !!event.currentTarget;
}
