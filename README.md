# EmailsEditor

<p align="center">
  <img
    src="./image.png"
    data-canonical-src="./image.png"
    width="540"
    height="300"
  />
</p>

[![pipeline status](https://gitlab.com/crazyzubrolet/emails-editor/badges/master/pipeline.svg)](https://gitlab.com/crazyzubrolet/emails-editor/commits/master)

## Description

This is a component that is implemented as a separate js script/library, so it can be used anywhere independently. It doesn't depend on any particular libraries and web applications. Because of that it allows anyone to use this library anywhere in their own applications. This plugin has public interface to communicate:

- `setEmails(string[], ?boolean)` - allows you to set emails in runtime. Second optional flag determines should it append to existing emails or replace them. `false` by default;
- `getEmails(?onlyValid)` - allows to retrieve emails from plugin. Optional flag will filter only valid emails. `false` by default; **NOTE:** see `EMAILS_CHANGE_EVENT`
- `EMAILS_CHANGE_EVENT` - custom event triggered on DOM throug plugin. It bubles so you can catch it on document. To get correct emails list via `getEmails` subscribe to this event to know when changes are applied;
- Via pulbic JS API
  - For more details see [example1.html](https://crazyzubrolet.gitlab.io/emails-editor/example1) and open the console;
- Custom WebComponent
  - For more details see [example2.html](https://crazyzubrolet.gitlab.io/emails-editor/example2) and open the console;

## Stack

- TypeScript to have type safe code and to provide external typings
- Rollup for bundling
  - PostCSS to process assets and CSS files
  - Terser plugin to optimize bundle
- Jest for testing
  - At the moment there are only unit tests for utilities
