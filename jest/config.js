const ignorePaths = ['<rootDir>/node_modules/'];

module.exports = {
  collectCoverageFrom: ['lib/**/*.{ts,tsx,js}'],
  coveragePathIgnorePatterns: [...ignorePaths, '<rootDir>/.*.d.ts'],
  coverageDirectory: '<rootDir>/.coverage',
  coverageReporters: ['lcov', 'text-summary'],
  globals: {
    'ts-jest': {
      tsConfig: 'tsconfig.test.json',
    },
  },
  testURL: 'http://localhost/',
  moduleFileExtensions: ['ts', 'tsx', 'js'],
  moduleNameMapper: {
    '\\.(woff2|ttf|eot|svg|png|jpe?g|gif)$': '<rootDir>/jest/mocks/resourceMock.js',
    '\\.css$': 'identity-obj-proxy',
    '^assets.*$': '<rootDir>/jest/mocks/resourceMock.js',
  },
  modulePaths: ['<rootDir>/lib'],
  resetModules: true,
  rootDir: '../',
  setupFiles: ['<rootDir>/jest/environment.js'],
  testRegex: 'lib/.*(.+\\.)?spec\\.tsx?$',
  testPathIgnorePatterns: ignorePaths,
  transform: {
    '^.+\\.tsx?$': 'ts-jest',
  },
  transformIgnorePatterns: [],
  verbose: true,
};
