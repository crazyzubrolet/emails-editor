declare module '*.css';
declare namespace EmailsEditor {
    type HTMLElementEvent<T extends HTMLElement> = Event & {
        target: T;
        currentTarget: T;
    };
    const enum KeyCode {
        ENTER = 13,
        SPACE = 32,
        COMMA = 188
    }
    const enum Events {
        EMAILS_CHANGE = "EMAILS_CHANGE_EVENT"
    }
}
//# sourceMappingURL=index.d.ts.map