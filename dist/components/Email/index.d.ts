interface Props {
    value: string;
    isValid?: boolean;
    onDelete: (email: string) => void;
    className?: string;
}
export declare const Email: (props: Props) => HTMLDivElement;
export {};
//# sourceMappingURL=index.d.ts.map