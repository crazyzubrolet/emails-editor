interface Props {
    container: HTMLElement;
    initialValues?: string[];
    placeholder?: string;
    className?: string;
}
export declare class TextArea {
    private props;
    private emails;
    private textArea;
    private validator;
    constructor(props: Props);
    setEmails(emails: string[], append?: boolean): void;
    getEmails(onlyValid?: boolean): string[];
    getEmailsCount(): number;
    private handleKeys;
    private fireChange;
    private onFocus;
    private onBlur;
    private recomputeEmails;
    private placeCaretAtTheEnd;
    private onDelete;
    private renderEmails;
    render(): void;
}
export {};
//# sourceMappingURL=index.d.ts.map