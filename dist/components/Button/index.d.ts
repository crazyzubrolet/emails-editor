interface ButtonProps {
    title: string;
    onClick: () => void;
    className?: string;
}
export declare const Button: (props: ButtonProps) => HTMLButtonElement;
export {};
//# sourceMappingURL=index.d.ts.map