interface Props {
    onAdd: (email: string) => void;
    onGetCount: () => void;
    className?: string;
}
export declare const ButtonsBlock: (props: Props) => HTMLDivElement;
export {};
//# sourceMappingURL=index.d.ts.map