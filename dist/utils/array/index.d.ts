export declare function unique<T = unknown>(array: T[]): T[];
export declare function diff<T = unknown>(array1: T[], array2: T[]): T[];
//# sourceMappingURL=index.d.ts.map