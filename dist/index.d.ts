interface Options {
    container: HTMLElement;
    boardName?: string;
    placeholder?: string;
    initialValues?: string[];
}
interface DomElementOptions extends DOMStringMap {
    boardname?: string;
    placeholder?: string;
    initialvalues?: string;
}
declare class App extends HTMLElement {
    dataset: DomElementOptions;
    private options;
    private textArea;
    constructor(options: Options);
    setEmails(emails: string[], append?: boolean): void;
    getEmails(onlyValid?: boolean): string[];
    render(): void;
}
export default function (...args: ConstructorParameters<typeof App>): App;
export {};
//# sourceMappingURL=index.d.ts.map